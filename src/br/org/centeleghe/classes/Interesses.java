package br.org.centeleghe.classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Interesses {
    public int idInteresse;
    public String nome;

    public void inserirInteresse(Usuario usuario) {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;

        String insertTableSQL = "INSERT INTO OO_interesse"
                + "(id_interesse, nome_interesse, id_usuario) VALUES"
                + "(seq_interesse.nextval,?,?)";

        try {
            String[] partes = nome.split(",");
            
            for(String texto : partes){

                
                String generatedColumns[] = {"id_interesse"};

                ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

                st = dbConnection.createStatement();

                ps.setString(1, texto);
                ps.setInt(2 , usuario.getIdUsuario());
                ps.executeUpdate();


                ResultSet rs = ps.getGeneratedKeys();
                int chaveGerada = 0;

                while (rs.next()) {
                    chaveGerada = rs.getInt(1);
                    System.out.println("id gerado: " + chaveGerada);
                }
                this.setIdInteresse(chaveGerada);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public int getIdInteresse() {
        return idInteresse;
    }

    public void setIdInteresse(int idInteresse) {
        this.idInteresse = idInteresse;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
}
