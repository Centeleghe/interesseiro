package br.org.centeleghe.classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Usuario {
    public int idUsuario;
    public String nome;
    public String senha;

    public Usuario inserirUsuario() {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;

        String insertTableSQL = "INSERT INTO OO_usuario"
                + "(id_usuario, nome_usuario, senha_usuario) VALUES"
                + "(seq_usuario.nextval,?,?)";

        try {

            String generatedColumns[] = {"id_usuario"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
            
            ps.setString(1, this.getNome());
            ps.setString(2, this.getSenha());
            ps.executeUpdate();


            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            this.setIdUsuario(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    
    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
}
