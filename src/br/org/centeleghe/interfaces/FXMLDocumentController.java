/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.centeleghe.interfaces;

import br.org.centeleghe.classes.Usuario;
import br.org.centeleghe.classes.Interesses;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    @FXML
    private TextField nomeTexto;
    @FXML
    private TextField senhaTexto;
    @FXML
    private Button cadastrarButton;
    @FXML
    private TextField interessesTexto;
    
    public void cadastrarButton(){
        Usuario usuario = new Usuario();
        Interesses interesses = new Interesses();
        
        usuario.setNome(nomeTexto.getText());
        usuario.setSenha(senhaTexto.getText());
        
        interesses.setNome(interessesTexto.getText());
        interesses.inserirInteresse(usuario.inserirUsuario());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
}
